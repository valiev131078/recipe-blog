from django.urls import path

from web.views import main_view, registration_view, auth_view, logout_view, recipe_edit_view, category_view, \
    recipe_delete_view

urlpatterns = [
    path('', main_view, name="main"),
    path('registration/', registration_view, name="registration"),
    path('auth/', auth_view, name="auth"),
    path('logout_view/', logout_view, name="logout"),
    path('recipe_edite_view/add/', recipe_edit_view, name="recipe_add"),
    path('recipe_edite_view/<int:id>/', recipe_edit_view, name="recipe_edite"),
    path('recipe_edite_view/<int:id>/delete/', recipe_delete_view, name="recipe_delete"),
    path('category/', category_view, name="category")
]
