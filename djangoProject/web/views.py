from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model, authenticate, login, logout

from web.forms import RegistrationForm, AuthForm, RecipeForm
from web.models import Recipe, Category

User = get_user_model()


@login_required
def main_view(request):
    recipe_blog = Recipe.objects.all().order_by("-created_at")
    return render(request, "web/main.html", {
        "recipe_blog": recipe_blog,
        "form": RecipeForm()

    })


def registration_view(request):
    form = RegistrationForm()
    is_success = False
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email']
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            print(form.cleaned_data)
            is_success = True
    return render(request, "web/registration.html", {"form": form, "is_success": is_success})


def auth_view(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, "Введены неверные данные")
            else:
                login(request, user)
                return redirect("main")
    return render(request, "web/auth.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("main")


# def recipe_view(request):
#     form = RecipeForm()
#     if request.method == 'POST':
#         form = RecipeForm(data=request.POST, initial={"user": request.user})
#         if form.is_valid():
#             form.save()
#             return redirect("main")
#     return render(request, "web/recipe.html", {"form": form})

@login_required
def recipe_edit_view(request, id):
    recipe = Recipe.objects.get(id=id) if id is not None else None
    form = RecipeForm(instance=recipe)
    if request.method == 'POST':
        form = RecipeForm(data=request.POST, files=request.FILES, instance=recipe, initial={"user": request.user})
        if form.is_valid():
            form.save()
            return redirect("main")
    return render(request, "web/recipe.html", {"form": form})


@login_required
def recipe_delete_view(request, id):
    recipe  = Recipe.objects.get(id=id)
    recipe.delete()
    return redirect("main")


def category_view(request):
    category = Category.objects.all()
    return render(request, "web/category.html", {"category": category})
