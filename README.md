#Recipe Blog

Проект-приме для демонстрации возможностей Django на онлайн курсе для КФУ

## Запуск проекта для разработчика

- 'python3 -m venv venv' - создание виртуального окружение
- 'venv\Scripts\activate' - войти в виртуальное окружение
- 'pip install -r requirements.txt' - установка зависимостей